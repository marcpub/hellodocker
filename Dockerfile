FROM ubuntu:latest

RUN set -x \
	&& apt update \
	&& apt upgrade --assume-yes \
	&& apt install --assume-yes \
		figlet \
		cowsay \
		lolcat

RUN set -x \
	&& rm -rf /var/lib/apt/lists/* \
	&& rm -rf /var/{cache,log}/*

COPY ./src/run.sh /run.sh

CMD ["sh", "/run.sh"]
