#!/bin/sh

export PATH="${PATH}:/usr/games"

figlet -f slant "Looks like your CI pipeline is working!!!" | cowsay -n -f turtle | lolcat
